﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace BarrCodes.Models
{
    public class BlogPost
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime DatePosted { get; set; }
        public string Content { get; set; }
        public string FeaturedImagesRef { get; set; }

        [NotMapped]
        public string Snippet
        {
            get
            {
                if (string.IsNullOrEmpty(Content)) return null;
                var subStrings = Regex.Split(Content, @"(<!--more)");
                return subStrings[0];
            }
        }

        [NotMapped]
        public List<Image> FeaturedImages
        {
            get
            {
                return JsonConvert.DeserializeObject<List<Image>>(FeaturedImagesRef);
            }
            set
            {
                FeaturedImagesRef = JsonConvert.SerializeObject(value);
            }
        }
    }
}