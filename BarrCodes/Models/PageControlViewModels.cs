﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BarrCodes.Models
{
    public class PageStepperData
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
    }
}