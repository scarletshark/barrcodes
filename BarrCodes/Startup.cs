﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BarrCodes.Startup))]
namespace BarrCodes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
