﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarrCodes.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //return View();
            // HACK - No homepage for now. I just need to get some shit online.
            return RedirectToAction("Index", "Blog", null);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}