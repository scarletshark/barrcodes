﻿using BarrCodes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BarrCodes.Controllers
{
    public class BlogController : Controller
    {
        private const int maxPostsPerPage = 10;
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index(int? page)
        {
            var posts = db.BlogPosts.AsEnumerable();
            var pageCount = posts.Count() / maxPostsPerPage;
            var currentPage = page.HasValue ? page.Value : 1;

            posts = posts.OrderByDescending(p => p.DatePosted)
                .Skip((currentPage - 1) * maxPostsPerPage)
                .Take(maxPostsPerPage);

            ViewBag.PageStepperData = new PageStepperData() { CurrentPage = currentPage, PageCount = pageCount };
            return View(posts.ToList());
        }


        public ActionResult Detail(int postID)
        {
            var post = db.BlogPosts
                .Where(p => p.ID == postID)
                .FirstOrDefault();

            ViewBag.Post = post;
            return View();
        }
    }
}