﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace BarrCodes.Utilities
{
    public static class HTTPUtility
    {
        public static string HTTPGet(string address)
        {
            var client = new WebClient();
            var stream = client.OpenRead(address);
            var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }
    }
}